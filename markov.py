import random

# License: MIT
# Author: Puck Meerburg <puck@puckipedia.nl>

# Usage:
# Create a markov.Collector(), then collector.collect(data.split(' ')) for each line of data
# then, " ".join(markov.Generator(collector)) - and you should have a markov string.

class Collector(object):
    def __init__(self, data = None):
        if data == None:
            data = {}
            data[None] = []
        self.data = data

    def add_item(self, prev, item):
        if prev != None:
            prev = prev.lower()

        if prev not in self.data:
            self.data[prev] = []
        self.data[prev].append(item)

    def get(self, index):
        if index != None:
            index = index.lower()
        return random.choice(self.data[index])

    def collect(self, data):
        previous_item = None
        for item in data:
            self.add_item(previous_item, item)
            previous_item = item
        self.add_item(data[-1], None)

class Generator(object):
    def __init__(self, data, ptr = None):
        self.data = data
        self.pointer = ptr
    def __next__(self):
        item = self.next()
        if item == None:
            raise StopIteration
        return item
    def next(self):
        next_item = self.data.get(self.pointer)
        self.pointer = next_item
        return next_item

    def __iter__(self):
        return self